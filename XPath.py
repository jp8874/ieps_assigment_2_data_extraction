import re


class MyXPath:

    def __init__(self):
        pass

    def parse_rtv_sites(self, tree):
        data = {}

        data['Title'] = tree.xpath('//*[@id="main-container"]//div/header/h1')[0].text
        data['SubTitle'] = tree.xpath('//*[@id="main-container"]//div/header/div[@class="subtitle"]')[0].text
        data['Lead'] = tree.xpath('//*[@id="main-container"]//div/header/p[@class="lead"]')[0].text
        data['Author'] = tree.xpath('//*[@id="main-container"]//div[@class="article-meta"]//div[@class="author-name"]')[0].text
        data['PublishedTime'] = tree.xpath('//*[@id="main-container"]//div[@class="article-meta"]//div[@class="publish-meta"]')[0].text.strip()
        article = tree.xpath('//*[@id="main-container"]//div/article/p/text()|//*[@id="main-container"]//div/article/p/strong/text()|//*[@id="main-container"]//div/article/p/sub/text()')
        content = ''
        for el in article:
            content += el + ' '
        data['Content'] = content

        return data

    def parse_overstock_sites(self, tree):
        data = {}
        items = []

        titles = tree.xpath('/html/body//table/tbody/tr/td[@valign="top"]/a/b/text()')
        listed_prices = tree.xpath('/html/body//table/tbody/tr/td/s/text()')
        prices = tree.xpath('/html/body//table/tbody/tr/td/span[@class="bigred"]/b/text()')
        savings = tree.xpath('/html/body//table/tbody/tr/td[@align="left"]/span[@class="littleorange"]/text()')
        content_text = tree.xpath('/html/body//table/tbody/tr/td/span[@class="normal"]/text()|/html/body//table/tbody/tr/td/span/a/span/b/text()')

        for i in range(len(titles)):
            data['Title'] = titles[i]
            data['ListPrice'] = listed_prices[i]
            data['Price'] = prices[i]
            pattern = re.compile("\$\d*,*\d*\.*\d*")
            data['Saving'] = pattern.search(savings[i])[0]
            pattern = re.compile("\([0-9]+%\)")
            data['SavingPercent'] = pattern.search(savings[i])[0]
            data['Content'] = str(content_text[i*2] + ' ' + content_text[i*2+1]).replace('\n', ' ')
            items.append(data)
            data = {}
        return items

    def parse_mimovrste_sites(self, tree):
        data = {}

        data['Title'] = tree.xpath('//*[@id="content"]/div/article/div/section/h1[@itemprop="name"]')[0].text
        data['Brand'] = tree.xpath('//*[@id="content"]/div/article/div//span/a[@class="link--pro-subtitle-info"]')[0].text
        data['Price'] = tree.xpath('//*[@id="content"]/div/article//div/b[contains(@class,"pro-price")]')[0].text.strip()
        data['ListPrice'] = tree.xpath('//*[@id="content"]/div/article//span/del')[0].text
        saving = tree.xpath('//*[@id="content"]/div/article//div/span[contains(@class,"base-price")]/text()')[0]
        pattern = re.compile("\d+,\d+ €")
        data['Saving'] = pattern.search(saving)[0]
        data['SavingPercent'] = tree.xpath('//*[@id="content"]/div/article//div/span/span[@class="moc-percentage"]')[0].text
        data['Content'] = tree.xpath(' //*[@id="content"]//div/section[@class="pro-column"]/p[@itemprop="description"]')[0].text.strip()
        technical_details = tree.xpath('//*[@id="product-detail-description"]//div[contains(@style, "background-color: rgb(229, 229, 229)")]//strong/text()|'
                                       '//*[@id="product-detail-description"]//ul/li/text()|//*[@id="product-detail-description"]//ul/li/strong/text()')
        content = ''
        for el in technical_details:
            content += el + ' '
        data['TechnicalDetails'] = content

        return data
