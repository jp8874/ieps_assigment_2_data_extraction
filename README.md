# IEPS assigment2: data extraction

- Project for WIER, masters course at FRI Faculty for computer and information science, University of Ljubljana in 2019.
- Authors: Julija Petrič (@JulijaPet), Ladislav Škufca (@ladislavskufca) and Tilen Venko (@tvenko).
- rules: http://zitnik.si/teaching/wier/PA2.html

# Basic description

The goal of this programming assignment is to implement three different approaches for the structured data extraction from the Web:

- Using regular expressions
- Using MyXPath
- RoadRunner-like implementation

# How to run

python DataExtraction.py 

