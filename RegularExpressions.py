import re


class RegularExpressions:

    def __init__(self):
        pass

    def preProcessing(self, content):
        # nadomesti prelome vrstic s presledkom
        content = re.sub(r'\r?\n|\r', " ", content)

        # več presledkov zamenjaj z enim
        content = ' '.join(content.split())
        return content

    def parse_rtv_sites(self, content):
        data = {}

        title_pattern = re.compile(r'<h1>(.*?)<\/h1>')
        data['Title'] = title_pattern.search(content).group(1)

        subtitle_pattern = re.compile(r'<div class=\"subtitle\">(.*?)<\/div>')
        data['SubTitle'] = subtitle_pattern.search(content).group(1)

        lead_pattern = re.compile(r'<p class=\"lead\">(.*?)<\/p>')
        data['Lead'] = lead_pattern.search(content).group(1)

        author_pattern = re.compile(r'<div class=\"author-name\">(.*?)<\/div>')
        data['Author'] = author_pattern.search(content).group(1)

        time_pattern = re.compile(
            r'(\d{1,2})\.\s(januar|februar|marec|april|maj|junij|julij|avgust|september|oktober|november|december)\s(\d{1,4})\sob\s(\d{1,2}:\d{1,2})')
        data['PublishedTime'] = time_pattern.search(content).group(0)

        content_pattern = re.compile(r'<article class=\"article\">.*?(<p>.*<\/p>|<p class=\"Body\">.*<\/p>).*?<\/article>')
        content_tmp = content_pattern.search(content).group(1)
        # pofiltriramo content da imamo besedilo
        # pobrišemo odvečne tage za izpis
        content_tmp = re.sub(r'<figure.*?<\/figure>', "", content_tmp)
        content_tmp = re.sub(r'<iframe.*?<\/iframe>', "", content_tmp)
        content_tmp = re.sub(r'<br>', " ", content_tmp)
        content_tmp = re.sub(r'<strong>', "", content_tmp)
        content_tmp = re.sub(r'<\/strong>', "", content_tmp)
        content_tmp = re.sub(r'<sub>', "", content_tmp)
        content_tmp = re.sub(r'<\/sub>', "", content_tmp)
        content_tmp = re.sub(r'<p class=\"Body\"><\/p>', "", content_tmp)
        content_tmp = re.sub(r'<p class=\"Body\">', "", content_tmp)
        content_tmp = re.sub(r'<p>', "", content_tmp)
        content_tmp = re.sub(r'<\/p>', " ", content_tmp)
        data['Content'] = content_tmp

        return data


    def parse_overstock_sites(self, content):
        data = {}
        items = []
        indeks = 0

        title_pattern = re.finditer(r'<td valign=\"top\">.<a href=.*?><b>(.*?)<\/b><\/a>.*?<\/td>', content)
        for title in title_pattern:
            data['Title'] = title.group(1)

            content_pattern = re.findall(r'<span class=\"normal\">(.*?)<\/span>', content)
            content_tmp = content_pattern[indeks]
            # pofiltriramo content da imamo besedilo
            # pobrišemo odvečne tage za izpis
            content_tmp = re.sub(r'<a href.*?<b>', "", content_tmp)
            content_tmp = re.sub(r'<\/b>', "", content_tmp)
            content_tmp = re.sub(r'<br>', " ", content_tmp)
            data['Content'] = content_tmp

            listPrice_pattern = re.findall(r'<td align=\"left\" nowrap=\"nowrap\"><s>(.*?)<\/s>', content)
            data['ListPrice'] = listPrice_pattern[indeks]

            price_pattern = re.findall(r'<td align=\"left\" nowrap=\"nowrap\"><span class=\"bigred\"><b>(.*?)<\/b><\/span><\/td>', content)
            data['Price'] = price_pattern[indeks]

            saving_pattern = re.findall(r'<td align=\"left\" nowrap=\"nowrap\"><span class=\"littleorange\">(.*?)\s\(\d*\%\)<\/span><\/td>', content)
            data['Saving'] = saving_pattern[indeks]

            savingPercent_pattern = re.findall(r'<td align=\"left\" nowrap=\"nowrap\"><span class=\"littleorange\">\$\d*\,*\d*\.*\d* (.*?)<\/span><\/td>', content)
            data['SavingPercent'] = savingPercent_pattern[indeks]

            items.append(data)
            data = {}
            indeks += 1


        return items

    def parse_mimovrste_sites(self, content):
        data = {}

        brand_pattern = re.compile(r'<span class=\"brand-info pro-subtitle-info-item hidden-mobile-inline\"> Znamka: <a href=.*?>(.*?)<\/a><\/span>')
        data['Brand'] = brand_pattern.search(content).group(1)

        content_pattern = re.compile(r'<p itemprop=\"description\" class=\".*?\">\s(.*?)\s<a href=.*?>Celoten opis<\/a><\/p>')
        content_pattern_tmp = content_pattern.search(content).group(1)
        # zamenjam &nbsp; v presledek
        content_pattern_tmp = re.sub(r'&nbsp;', " ", content_pattern_tmp)
        data['Content'] = content_pattern_tmp

        listPrice_pattern = re.compile(r'<div class=\"pro-price-wraper \"><b class=\"pro-price con-emphasize font-primary--bold mr-5\">.*?<del>(.*?)<\/del>.*?<\/b>.*?<\/div>')
        data['ListPrice'] = listPrice_pattern.search(content).group(1)

        price_pattern = re.compile(r'<div class=\"pro-price-wraper \"><b class=\"pro-price con-emphasize font-primary--bold mr-5\">\s(.*?)\s<\/b>.*?<\/div>')
        data['Price'] = price_pattern.search(content).group(1)

        saving_pattern = re.compile(r'<div class=\"pro-price-wraper \"><b class=\"pro-price con-emphasize font-primary--bold mr-5\">.*?<\/del>,\sPrihranite\s(.*?)\s<span.*?.*?<\/div>')
        data['Saving'] = saving_pattern.search(content).group(1)

        savingPercent_pattern = re.compile(r'<div class=\"pro-price-wraper \">.*?<span class=\"moc-percentage\">(.*?)<\/span.*?<\/div>')
        savingPercent_tmp = savingPercent_pattern.search(content).group(1)
        # zamenjam &nbsp; v presledek
        savingPercent_tmp = re.sub(r'&nbsp;', " ", savingPercent_tmp)
        data['SavingPercent'] = savingPercent_tmp

        brand_pattern = re.compile(r'<h1 itemprop=\"name\" class=\"lay-overflow-hidden word-break--word mt-5\">(.*?)<\/h1>')
        data['Title'] = brand_pattern.search(content).group(1)

        technical_details = re.finditer(r'<span style=\"color: black; font-weight: bold;\">.*?<strong>(.*?)<\/strong>', content)
        indeks = 0
        tmp_technical_details = ''
        for detail in technical_details:
            tmp_technical_details += detail.group(1)

            details_pattern = re.findall(r'<ul>(.*?)<\/ul>', content)
            tmp_technical_details += details_pattern[indeks]
            indeks += 1

        # odstranim odvečne tage
        tmp_technical_details = re.sub(r'<li><strong>', " ", tmp_technical_details)
        tmp_technical_details = re.sub(r'<li>', " ", tmp_technical_details)
        tmp_technical_details = re.sub(r'<\/li>', "", tmp_technical_details)
        tmp_technical_details = re.sub(r'<strong>', "", tmp_technical_details)
        tmp_technical_details = re.sub(r'<\/strong>', "", tmp_technical_details)
        # zamenjam &nbsp; v presledek
        tmp_technical_details = re.sub(r'&nbsp;', " ", tmp_technical_details)
        # zamenjam &amp v &
        tmp_technical_details = re.sub(r'&amp;', "&", tmp_technical_details)
        data['TechnicalDetails'] = tmp_technical_details

        return data
