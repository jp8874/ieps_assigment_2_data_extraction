﻿import re
import lxml
from lxml.html.clean import Cleaner
from lxml.html import fromstring, tostring
from html.parser import HTMLParser

class MyHTMLParser(HTMLParser):
	
	x = list()
	depth = 0
	
	def handle_starttag(self, tag, attrs):
		self.depth = self.depth + 1
		self.x.append(["start_tag", tag, self.depth])
		if tag == "img" or tag == "br":
			self.depth = self.depth - 1
		
	def handle_endtag(self, tag):
		self.x.append(["end_tag", tag, self.depth])
		self.depth = self.depth - 1
		
	def handle_data(self, data):
		if  "\n" not in data and "\t" not in data:
			self.x.append(["data", data, self.depth])
			

class MyHTMLParser2(HTMLParser):
	
	x = list()
	depth = 0
	
	def handle_starttag(self, tag, attrs):
		self.depth = self.depth + 1
		self.x.append(["start_tag", tag, self.depth])
		if tag == "img" or tag == "br":
			self.depth = self.depth - 1
		
	def handle_endtag(self, tag):
		self.x.append(["end_tag", tag, self.depth])
		self.depth = self.depth - 1
		
	def handle_data(self, data):
		if  "\n" not in data and "\t" not in data:
			self.x.append(["data", data, self.depth])

class MyRoadRunner:

	def __init__(self):
		pass
		
	@staticmethod
	def tag_mismatch(x, y, i, j, n):
		same = []
		start = x[i][1]
		while i < n:
			if x[i][2] == y[j][2] and x[i][0] == "start_tag" and y[j][0] == "start_tag" and (x[i][1] == y[j][1] or 
			(x[i][1] == "b" and y[j][1] == "a") or (x[i][1] == "a" and y[j][1] == "b")):
				same.append("<" + x[i][1] + ">")					
			elif x[i][2] == y[j][2] and x[i][0] == "data" and y[j][0] == "data" and x[i][1] == y[j][1]:
				#same.append(x[i][1])
				same.append("#text")
			elif x[i][2] == y[j][2] and x[i][0] == "data" and y[j][0] == "data" and x[i][1] != y[j][1]:
				same.append("#text")
			elif x[i][2] == y[j][2] and x[i][0] == "end_tag" and y[j][0] == "end_tag" and x[i][1] == y[j][1]:
				same.append("</" + x[i][1] + ">")					
				if start == x[i][1]:
					break
			elif x[i][2] == y[j][2] and x[i][0] == "data" and y[j][0] == "start_tag" and (y[j][1] == "a" or y[j][1] == "b"):
				same.append("#text")
				j = j + 2
			else:
				same = []
				break
			j = j + 1
			i = i + 1
		return [same, j]
	
	@staticmethod	
	def tag_mismatches(x, n):
		i = 0
		r = []
		while i < n:
			if x[i][0] == "start_tag":
				t1 = x[i][1]
				d1 = x[i][2]
				same = []
				for j in range(i+1, n):
					if x[j][2] < d1:
						break
					if x[j][0] == "start_tag":
						if t1 == x[j][1] and d1 == x[j][2]:
							same = MyRoadRunner.tag_mismatch(x, x, i, j, n)
				if len(same) == 0 or (len(same) == 2 and len(same[0]) == 0):
					r.append(["<" + x[i][1] + ">", x[i][2]])
				else:
					i = same[1]
					r.append([same[0], 0])
			elif x[i][0] == "data":
				r.append([x[i][1], x[i][2]])
			elif x[i][0] == "end_tag":
				r.append(["</" + x[i][1] + ">", x[i][2]])		
			i = i + 1
		return r
   
	def runner(self, tree1, tree2):
		data = {}
		
		cleaner = Cleaner()
		cleaner.javascript = True
		cleaner.style = True
		cleaner.page_structure = False
		
		
		wrapper_page = MyHTMLParser()
		wrapper_page.feed(cleaner.clean_html(tostring(tree1)))
		
		
		sample_page = MyHTMLParser2()
		sample_page.feed(cleaner.clean_html(tostring(tree2)))
			
		r1 = MyRoadRunner.tag_mismatches(wrapper_page.x, len(wrapper_page.x))				
		r2 = MyRoadRunner.tag_mismatches(sample_page.x, len(sample_page.x))
		
		i = 0
		j = 0
		while i < len(r1) and j < len(r2):
			if r1[i][0] == r2[j][0] and r1[i][1] == r2[j][1]:
				if isinstance(r1[i][0], list):
					print("(", end=' ')
					for k in r1[i][0]:
						print(k, end=' ')
					print(")+")

				else:
					print(r1[i][0])

			elif "<" not in r1[i][0] and "<" not in r2[j][0] and r1[i][0] != r2[j][0] and r1[i][1] == r2[j][1]:
				print("#text")
			else:
				m = 1
				while m < 50:
					k = j
					table = []
					while k < len(r2):
						if r1[i][0] == r2[k][0] and r1[i][1] == r2[k][1] and r1[i+1][0] == r2[k+1][0] and r1[i+1][1] == r2[k+1][1]:
							break
						table.append(r2[k])
						k = k + 1
						if k > j + m:
							table = []
							break
					if len(table) > 0:
						print("(", end=' ')
						for l in table:
							if isinstance(l, list):
								for ll in l:
									print(ll, end=' ')
							else:
								print(l, end=' ')
						print(")?")
						j = k
						break
					if len(table) == 0:
						k = i
						table = []
						while k < len(r1):
							if r1[k][0] == r2[j][0] and r1[k][1] == r2[j][1] and r1[k+1][0] == r2[j+1][0] and r1[k+1][1] == r2[j+1][1]:
								break
							table.append(r1[k])
							k = k + 1
							if k > i + m:
								table = []
								break
						if len(table) > 0:
							print("(", end=' ')
							for l in table:
								if isinstance(l, list):
									for ll in l:
										print(ll, end=' ')
								else:
									print(l, end=' ')
							print(")?")
							i = k
							break
					m = m + 1
					
			i = i + 1
			j = j + 1
			
		return data
