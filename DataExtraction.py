﻿from XPath import MyXPath
from RegularExpressions import RegularExpressions
from RoadRunner import MyRoadRunner
from lxml import html
import json

rtv_sites = [
    'WebPages/rtvslo.si/Audi A6 50 TDI quattro_ nemir v premijskem razredu - RTVSLO.si.html',
    'WebPages/rtvslo.si/Volvo XC 40 D4 AWD momentum_ suvereno med najboljše v razredu - RTVSLO.si.html',
]
overstock_sites = [
    'WebPages/overstock.com/jewelry01.html',
    'WebPages/overstock.com/jewelry02.html',
]
mimovrste_sites = [
    'WebPages/mimovrste.com/prenosnik.html',
    'WebPages/mimovrste.com/sesalnik.html'
]


def read_document(file_name):
    try:
        html_document = open(file_name, 'r', encoding='utf-8').read()
        return html.fromstring(html_document)
    except:
        try:
            html_document = open(file_name, 'r', encoding='windows-1252').read()
            return html.fromstring(html_document)
        except:
			try:
				html_document = open(file_name, 'r').read()
				return html.fromstring(html_document)
			except:
				print('File can not be read')
				return None


def read_and_transform_document(file_name):
    """

    :param file_name: path and name of the html document
    :return: parsed html
    """
    html_document = read_document(file_name)
    if html_document is not None:
        return html.fromstring(html_document)
    return None


def print_json(data):
    print(json.dumps(data, indent=4, sort_keys=True, ensure_ascii=False))


if __name__ == '__main__':
    x_path = MyXPath()
    regex = RegularExpressions()
    road_runner = MyRoadRunner()
    
    #print('--- ROAD RUNNER ---')
    road_runner.runner(read_and_transform_document(rtv_sites[0]), read_and_transform_document(rtv_sites[1]))
    road_runner.runner(read_and_transform_document(overstock_sites[0]), read_and_transform_document(overstock_sites[1]))
    road_runner.runner(read_and_transform_document(mimovrste_sites[0]), read_and_transform_document(mimovrste_sites[1]))

    print('--- RTV sites regex ---')
    for site in rtv_sites:
        document = read_document(site)
        document = regex.preProcessing(document)
        print_json(regex.parse_rtv_sites(document))

    print('\n\n--- Overstock sites regex ---')
    for site in overstock_sites:
        document = read_document(site)
        document = regex.preProcessing(document)
        print_json(regex.parse_overstock_sites(document))

    print('\n\n--- Mimovrste sites regex ---')
    for site in mimovrste_sites:
        document = read_document(site)
        document = regex.preProcessing(document)
        print_json(regex.parse_mimovrste_sites(document))

    print('--- RTV sites xPath ---')
    for site in rtv_sites:
        tree = read_and_transform_document(site)
        print_json(x_path.parse_rtv_sites(tree))

    print('\n\n--- Overstock sites xPath ---')
    for site in overstock_sites:
        tree = read_and_transform_document(site)
        if tree is not None:
            print_json(x_path.parse_overstock_sites(tree))

    print('\n\n--- Mimovrste sites xPath ---')
    for site in mimovrste_sites:
        tree = read_and_transform_document(site)
        print_json(x_path.parse_mimovrste_sites(tree))
